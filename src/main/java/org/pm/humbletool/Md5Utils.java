package org.pm.humbletool;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author pm
 */
public class Md5Utils {

    public static byte[] checksum(Path path) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            throw new IllegalArgumentException(ex);
        }

        try ( InputStream is = new FileInputStream(path.toString());  DigestInputStream dis = new DigestInputStream(is, md)) {
            // while (dis.read() != -1);

            byte[] byteArray = new byte[1024];
            var bytesCount = 0;

            while ((bytesCount = dis.read(byteArray)) != -1) {
                dis.read(byteArray, 0, bytesCount);
            }

            md = dis.getMessageDigest();
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }

        return md.digest();
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

}
