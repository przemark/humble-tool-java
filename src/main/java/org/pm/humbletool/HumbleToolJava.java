package org.pm.humbletool;

import java.nio.file.InvalidPathException;
import java.nio.file.Paths;

/**
 *
 * @author pm
 */
public class HumbleToolJava {

    public static void main(String[] args) {
        System.out.println("Humble Tool in Java\n");

        if (args.length != 2) {
            System.out.println("Please give dir and log file.");
            System.exit(1);
        }

        try {
            var workingDir = Paths.get("").toAbsolutePath().toString();
            System.out.println("Working dir: " + workingDir);
            var dirPath = Paths.get(args[0]);
            System.out.println("Dir: " + dirPath);
            var logPath = Paths.get(args[1]);
            System.out.println("Log path: " + logPath);

            var ht = new HumbleTool();
            var result = ht.checkAllMd5(dirPath);
            ht.saveToFile(result, logPath);
            System.out.println(ht.getFormattedResult(result));
        } catch (InvalidPathException ex) {
            System.out.println("Bad dir or file log name.");
            System.exit(1);
        }

    }
}
