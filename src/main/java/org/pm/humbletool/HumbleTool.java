package org.pm.humbletool;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Stream;

/**
 *
 * @author pm
 */
public class HumbleTool {

    protected record Md5Result(String dir, String file, boolean correct, String error) {

        @Override
        public String toString() {
            return """
            %s \t %s %b
            """.formatted(dir, file, correct);
        }
    }

    public List<List<Md5Result>> checkAllMd5(Path path) {
        System.out.println("checkAllMd5s");

        var processors = Runtime.getRuntime().availableProcessors();
        var exec = Executors.newFixedThreadPool(processors);
        List<Path> dirs = null;
        try ( Stream<Path> entries = Files.list(path)) {
            dirs = entries.toList();
        } catch (IOException ex) {
            System.out.println("Error: read dirs");
            System.exit(1);
        }

        var tasks = dirs.stream()
                .map(p -> startTask(exec, p))
                .toList();

        var result = tasks.stream()
                .map(x -> this.getMd5Result(x, path))
                .toList();
        exec.shutdown();
        return result;

    }

    private List<Md5Result> checkMd5FromFile(Path path) throws UncheckedIOException {
        var pathToMd5File = path.resolve(path.getFileName().toString() + ".md5");
        var result = new ArrayList<Md5Result>();
        try {
            try ( Stream<String> lines = Files.lines(pathToMd5File, StandardCharsets.UTF_8)) {
                lines.forEach(row -> {
                    var md5File = row.split("  ");
                    var md5 = md5File[0];
                    var file = md5File[1];
                    var calculatedSum = Md5Utils.bytesToHex(Md5Utils.checksum(path.resolve(file)));
                    result.add(new Md5Result(path.toString(), file, md5.equals(calculatedSum), null));
                }
                );
            }
        } catch (IOException ex) {
            result.add(new Md5Result(path.toString(), null, false, "Error: read md5 file"));
        }
        return result;
    }

    private List<Md5Result> getMd5Result(Future<List<Md5Result>> x, Path dir) {
        try {
            return x.get();
        } catch (InterruptedException | ExecutionException ex) {
            var result = new Md5Result(dir.toString(), null, false, "Error: execution task");
            var list = new ArrayList<Md5Result>();
            list.add(result);
            return list;
        }
    }

    private Future<List<Md5Result>> startTask(ExecutorService exec, Path p) {
        return exec.submit(() -> {
            System.out.println("Start task: " + p);
            var result = checkMd5FromFile(p);
            System.out.println("End task: " + p);
            return result;
        });
    }

    public void saveToFile(List<List<Md5Result>> result, Path logFile) {
        String getFormattedText = getFormattedResult(result);

        try {
            Files.write(logFile, getFormattedText.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException ex) {
            System.out.println("Error: save to file " + logFile);
        }
    }

    public String getFormattedResult(List<List<Md5Result>> result) {
        String text = """
                              --------------------------------------------\n
                              Date: %s
                              %s
                              Status(check dir/correct dir): %d/%d
                              --------------------------------------------\n
                              """;

        var strResult = result.stream()
                .flatMap(x -> Stream.concat(Stream.of("\n"), x.stream().map(y -> y.toString())))
                .reduce("", (a, b) -> a.concat(b));
        var correct = result.stream()
                .filter(x -> x.size() == x.stream().filter(y -> y.correct).count())
                .count();

        return text.formatted(new Date(), strResult, result.size(), correct);
    }

}
